#This is a driver code that invokes the lambda function
import AccessErrorCodes
import os
import json
#The event object carries the needed parameters to the lambda function
event = ({"ErrorCode":0,"Manufacturer": "Signet","Model":"Signet C"})
context = "Test"
print(json.dumps(AccessErrorCodes.VendorErrorTest(event, context)))